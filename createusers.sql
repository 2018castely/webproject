CREATE TABLE Users (
    user_id INTEGER PRIMARY KEY AUTOINCREMENT,
    username CHAR(20) NOT NULL,
    password CHAR(60) NOT NULL,
    salt CHAR (40) NOT NULL,
    addresse_mail CHAR(64) NOT NULL
);

Réducteur d'URL : un site web proposant à ses utilisateurs authentifiés de créer des URL réduites et de récupérer diverses informations et statistiques sur leur utilisation. Diverses améliorations peuvent être imaginées, comme la génération de QR codes ou la prévisualisation d'un site cible au sein de l'application.

Le site du projet, hébergé par le Rézo Rennes:
https://flaskurl.rez-rennes.fr:5000/

Pour lancer le docker : 
```
docker build .
docker run --rm -d -p 5000:5000 <IMAGE>
```
Pour avoir un shell dans le conteneur :
```
sudo docker exec -it <CONTAINER_ID> bash
```
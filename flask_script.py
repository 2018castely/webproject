from flask import Flask
from flask import redirect
from flask import abort
from flask import request
from flask import render_template
from flask import url_for
from flask import flash
import bcrypt
import socket
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
import flask_login
from flask_login import LoginManager, login_required, current_user, login_user, UserMixin
from sqlalchemy.orm import scoped_session, sessionmaker
from flask_ipban import IpBan
from MyQR import myqr
from datetime import timedelta
import random
import sys
import os
import urllib

alphabet = "abcdefghijklmnopqrstuvwxyz"
LENGTH_SHORT_URL = 8

app = Flask(__name__, static_url_path='/static')

login_manager = flask_login.LoginManager()
login_manager.init_app(app)
app.config['SECRET_KEY'] = os.urandom(24)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
engine = create_engine('sqlite:///database.db', echo=False, connect_args={'check_same_thread': False})
conn = engine.connect()
db = SQLAlchemy(app)

Session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))
#Session.configure(bind=engine)
session = Session()
ip_ban = IpBan(ban_seconds=300, ban_count = 20)
ip_ban.init_app(app)

class URL(db.Model):
    __tablename__ = 'URLsTable'
    id = db.Column('id', db.Integer, primary_key = True)
    original_url = db.Column(db.String(2000))
    shortened_url = db.Column(db.String)
    number_of_clicks = db.Column(db.Integer)
    owner_id = db.Column(db.Integer)

class Users(UserMixin, db.Model):
    __tablename__ = 'Users'
    user_id = db.Column('user_id', db.Integer, primary_key = True)
    username = db.Column(db.String(20))
    password = db.Column(db.String(60))
    salt = db.Column(db.String(40))
    addresse_mail = db.Column(db.String(64))
    def is_active(self):
         return self.is_active()    
    def is_anonymous(self):
         return False    
    def is_authenticated(self):
         return self.authenticated       
    def get_id(self):
         return self.user_id

@app.route("/index.html")
def redirect_home2():
    return redirect(url_for("render_home"))   

@app.route("/index")
def redirect_home():
    return redirect(url_for("render_home"))    

@app.route("/")
def render_home():
    
    if current_user.is_authenticated:
        return redirect(url_for('shorten_url'))        
    else:
        return render_template("index.html")
    

@app.route("/signout")
def signout():
    flask_login.logout_user()
    return render_template("index.html")

@app.route("/subscribe.html")
def redirect_subscribe():
    return redirect(url_for("subscribe")) 

@app.route("/subscribe", methods = ['POST', 'GET'])
def subscribe():
    try:
        if request.method == 'POST':
            username = request.form['username']
            password = request.form['password']
            email_r = request.form['email']
            email = email_r.lower()
            if (session.query(Users).filter(Users.username==username).first() != None or session.query(Users).filter(Users.addresse_mail==email).first() != None ) :
                flash("Username or email address already exists")
                return render_template("subscribe.html")
            else:
                salt = bcrypt.gensalt()
                hashed = bcrypt.hashpw(password.encode('utf-8'), salt)
                user = Users(username = username, password = hashed, salt = salt, addresse_mail = email)
                session.add(user)
                session.commit()
                return redirect(url_for('render_home'))
        else:
            return render_template("subscribe.html")
    except Exception as e:
        return render_template("subscribe.html")
    
@app.route("/login.html")
def redirect_login():
    return redirect(url_for("login")) 

userID = 0

@app.route("/login", methods = ['POST', 'GET'])
def login():
    try:        
        if request.method == 'POST':
            #hostname = socket.gethostname()
            #ip_address = socket.gethostbyname(hostname)
            #print(ip_address)

            username = request.form['username']
            password = request.form['password']
            if (username == '' or password == ''):
                flash('Please enter username or password!') 
                ip_ban.add()
                return render_template("login.html")
            else:
                if session.query(Users).filter_by(username=username).first() != None :
                    salt = str(session.query(Users.salt).filter(Users.username == username).first())
                    hashed = bcrypt.hashpw(password.encode('utf-8'), salt[3:-3].encode('utf-8'))
                    if session.query(Users).filter(Users.username==username, Users.password==hashed).first() != None :
                        user = session.query(Users).filter(Users.username==username, Users.password==hashed).first()
                        flask_login.login_user(user, remember=True)
                        if current_user.is_authenticated:
                            flash('Sucessfully logged in')
                            return redirect(url_for('shorten_url'))
                    else:
                        flash('Wrong username or password !') 
                        ip_ban.add()
                        return redirect(url_for('login'))  
                else :
                    ip_ban.add()
                    flash('Wrong username or password !') 
                    return redirect(url_for('login')) 
        else:
            return render_template("login.html")
    except Exception as e:
        flash("error")
        return render_template("login.html")


@app.route("/shorten", methods = ['POST', 'GET'])
@login_required
def shorten_url():
    try:
        if request.method == 'POST':
            if current_user.is_authenticated == False:
                flash('Not authenticated')
                return redirect(url_for('login'))   
            else:
                original_url = request.form['input-url']
                if (original_url.lower().startswith('https://') == False and original_url.lower().startswith('http://') == False):
                    flash('Please enter a valid URL!')
                    return render_template("shorten.html")
                else:
                    original_url_inter = original_url.split(":",1)
                    original_url = original_url_inter[0].lower()+":"+original_url_inter[1]
                    cursor = session.query(URL).filter_by(original_url = original_url).first()
                    
                    try:
                        urllib.request.urlopen(original_url)
                    except Exception as e:
                        flash('Please enter a valid URL!')
                        return render_template("shorten.html")
                    if cursor == None :
                        shortened_url =  generate_short_url()
                        
                        while session.query(URL).filter_by(shortened_url = shortened_url).first() != None :
                            shortened_url =  generate_short_url()
                        new_url = URL (original_url = original_url, shortened_url = shortened_url, number_of_clicks = 0, owner_id = int(current_user.user_id))
                        session.add(new_url)
                        session.commit()
                        
                        return render_template("results.html", shortened_url = "https://0.0.0.0:5000/"+shortened_url, qr_code_file = generate_qr_code(shortened_url))
                    else:
                        return render_template("results.html", shortened_url = "https://0.0.0.0:5000/"+cursor.shortened_url, qr_code_file = generate_qr_code(cursor.shortened_url))

        else:
            if current_user.is_authenticated == False:
                flash('Not authenticated!')
                return redirect(url_for('login')) 
            else:
                return render_template("shorten.html")
    except Exception as e:
        print(e)
        return render_template("shorten.html")


@app.route("/<shortened_url>")
def redirect_url_shortened(shortened_url):
    
    cursor = URL.query.filter_by(shortened_url = shortened_url).first()
    if cursor != None :
        url = session.query(URL).filter_by(shortened_url = shortened_url).first() 
        url.number_of_clicks += 1
        
        session.commit()
        return render_template("wait_before_redirect.html", original_url = cursor.original_url)

    else:
        return abort(404)
    

@app.errorhandler(401)
def page_unauthorized(e):
    return render_template("401.html")
    
@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html")



@app.route("/stats")
@login_required

def stats():

    if current_user.is_authenticated == False:
        flash('Not authenticated')
        return redirect(url_for('login'))   
    else:
        userid = int(current_user.user_id)

        #print(userid)
        urls = session.query(URL).filter_by(owner_id = userid).all()
        #print(urls)

        if urls != None :
            return render_template("stats.html", url= urls)
        else:
            return render_template("stats.html")
            

def generate_short_url(): #Generate string of size LENGTH_SHORT_URL with following characters : a-z,A-Z,0-9

    short_url = ""

    for i in range(LENGTH_SHORT_URL):
        rand = random.randint(0, 61)
        if 0 <= rand <= 25:
            short_url += alphabet[rand]
        elif 26 <= rand <= 51:
            short_url += alphabet[rand-26].upper()
        else:
            short_url += str(rand-52)
 
    return short_url


def generate_qr_code(shortened_url):
    file_name = "qr_"+shortened_url+".jpg"
    myqr.run("https://0.0.0.0:5000/"+shortened_url,
    version = 1,
    level = 'H',
    picture = None,
    colorized = False,
    contrast = 1.0,
    brightness = 1.0,
    save_name = file_name,
    save_dir = os.getcwd()+"/static/images/qr_codes/")
    return "static/images/qr_codes/"+file_name

@login_manager.user_loader
def load_user(user_id):


    return session.query(Users).filter(Users.user_id == user_id).first()

@app.before_request
def before_request():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=10) 


#@app.teardown_appcontext
#def shutdown_session(exception=None):
#    Session.remove()

if __name__ == "__main__":
    
    app.run(debug = True, host='0.0.0.0', port=5000)


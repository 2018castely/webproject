import sqlite3
import sqlalchemy
from sqlalchemy import create_engine

connection = sqlite3.connect('database.db')
#engine = create_engine('sqlite:///database.db', echo=True)
with open('createusers.sql') as f:
    connection.executescript(f.read())
with open('createtable.sql') as f:
    connection.executescript(f.read())

                                
connection.commit()
connection.close()


window.onload = function () {
  document.getElementById("contact").onsubmit = function () {
    return checkUsername() && checkEmail() && checkPassword() && matchPassword() ;
  };
  document.getElementById("username").onblur = checkUsername;
  document.getElementById("email").onblur = checkEmail;
  document.getElementById("password1").onblur = checkPassword;
  document.getElementById("password2").onblur = matchPassword;
  document.get
};

function checkUsername() {
  var username = document.getElementById("username").value;
  var reg_username = /[0-9A-Za-z]{3,15}$/;
  var flag = reg_username.test(username);
  var s_username = document.getElementById("s_username");
  var c_username = document.getElementById("c_username");
  if (flag) {
    s_username.innerHTML = "<span style='color:green'>&#x2714</span>";
  } else {
    s_username.innerHTML = "&#10060";
    alert("\nInvalid username format: its length should more than 3");
  }
  return flag;
}

function checkEmail() {
  var email = document.getElementById("email").value;
  var reg_password = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
  var flag = reg_password.test(email);
  var s_email = document.getElementById("c_email");

  if (flag) {
    s_email.innerHTML = "<span style='color:green'>&#x2714</span>";
  } else {
    s_email.innerHTML = "&#10060";
    alert("\nInvalid email format");   
  } 
  return flag;
}


function checkPassword() {
  var password1 = document.getElementById("password1").value;
  var reg_password = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,30}$/;
  var flag = reg_password.test(password1);
  var s_passworld1 = document.getElementById("s_password1");

  if (flag) {
    s_passworld1.innerHTML = "<span style='color:green'>&#x2714</span>";
  } else {
    s_passworld1.innerHTML = "&#10060";
    alert("\nInvalid password format: it should include at least 8 letters in which letter and number exist");   
  }
  return flag;
}

function matchPassword() {
  var password1 = document.getElementById("password1").value;
  var password2 = document.getElementById("password2").value;

  var s_passworld2 = document.getElementById("s_password2");
  var flag = false;

  if (password2 == password1 && password2 != "") {
    s_passworld2.innerHTML = "<span style='color:green'>&#x2714</span>";
    flag = true;
  } else {
    s_passworld2.innerHTML = "&#10060";
    alert("\nPasswords did not match: Please try again");
  }
  return flag;
}

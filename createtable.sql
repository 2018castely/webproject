CREATE TABLE URLsTable (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    original_url CHAR(2000) NOT NULL,
    shortened_url STRING NOT NULL,
    number_of_clicks INTEGER NOT NULL DEFAULT 0,
    owner_id INTEGER NOT NULL 
);
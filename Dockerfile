FROM python:latest

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

EXPOSE 5000
 
CMD python flask_script.py 
 
